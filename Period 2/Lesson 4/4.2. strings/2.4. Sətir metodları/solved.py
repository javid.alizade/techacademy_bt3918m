# -*- coding: utf-8 -*-

### Funksiyalar haqda daha ətraflı python dilinin rəsmi dokumentasiya səhifəsindən öyrənə bilərsiniz:
### Link: https://docs.python.org/2/library/string.html#string-functions


myStr = "salam dünya"

# 1. capitalize() - Sətrin ilk hərfini böyüdür
print("1. myStr.capitalize() =>", myStr.capitalize())

# 2. center(width, fillchar) - width parametrində verilmiş rəqəmdən səntrin uzunluğunu çıxıb qalıq sayı qədər sətrin əvvəlinə və axırına fillchar simvolunu artırır
print("2. myStr.center(20, '-') =>", myStr.center(20, '-'))

# 3. count(str, beg = 0,end = len(string))
print("3. myStr.count('a') =>", myStr.count('a'))
print("3. myStr.count('a', 3) =>", myStr.count('a', 3))
print("3. myStr.count('a', 3, 7) =>", myStr.count('a', 3, 7))

# 4. encode(encoding = 'UTF-8',errors = 'strict') - Sətri byte tipinə çevirir
encodedStr = myStr.encode('UTF-8')
print("4. myStr.encode('UTF-8') =>", encodedStr)

# 5. decode(encoding = 'UTF-8',errors = 'strict') - Encode olunmuş byte tipini sətrə çevirir
print("5. encodedStr.decode('UTF-8') =>", encodedStr.decode('UTF-8'))

# 6. endswith(suffix, beg = 0, end = len(string)) - Sətrin suffix parametrindəki sözlə bitdiyini yoxlayır
print("6. myStr.endswith('ya') =>", myStr.endswith('ya'))
print("6. myStr.endswith('yalan') =>", myStr.endswith('yalan'))

# 7. isalnum() - Əgər sətir yalnızca rəqəm və hərflərdən ibarətdirsə onda True qaytarır. Əks halda False qaytaracaq
print("7. 'abc123'.isalnum() =>", 'abc123'.isalnum())
print("7. 'abc-123'.isalnum() =>", 'abc-123'.isalnum())

# 8. find(str, beg = 0 end = len(string)) - str parametrinə verilən sətri digər sətrin içərisində axtarır. Əgər tapılsa onda yerləşdiyi yerin indeksini qaytarır, əks halda -1 qaytaracaq. Əgər beg və end verilsə onda həmən aralıqda str mətni axtarılacaq.
print("8. 'lorem ipsum dolor amed'.find('or') =>", 'lorem ipsum dolor amed'.find('or'))
print("8. 'lorem ipsum dolor amed'.find('yalan') =>", 'lorem ipsum dolor amed'.find('yalan'))

# 9. index(str, beg = 0, end = len(string)) - find ilə eynidir. Lakin sətir tapılmadığı zaman xəta çıxarır
print("9. 'lorem ipsum dolor amed'.index('or') =>", 'lorem ipsum dolor amed'.index('or'))

# 10. expandtabs(tabsize = 10) - Mətndəki tab simvollarını tabsize qədər space ilə əvəz edir
print("10. 'Lorem ipsum dolor   amed'.expandtabs(tabsize=10) =>", 'Lorem ipsum dolor   amed'.expandtabs(tabsize=10))

# 11. isalpha() - Əgər sətrir yalnızca hərflərdən ibarətdirsə onda True qaytarır. Əks halda False qaytarır
print("11. 'ancaqherfler'.isalpha() =>", 'ancaqherfler'.isalpha())
print("11. 'lorem ipsum'.isalpha() =>", 'lorem ipsum'.isalpha())

# 12. isdigit() - Əgər sətir yalnızca rəqəmlərdən ibarətdirsə onda True qaytarır. Əks halda False qaytarır
print("12. '1235435'.isdigit() =>", '1235435'.isdigit())

# 13. islower() - Əgər sətirdəki hərflər kiçikdirsə onda True qaytarır
print("13. 'salam dünya'.islower() =>", 'salam dünya'.islower())
print("13. 'Salam Dünya'.islower() =>", 'Salam Dünya'.islower())

# 14. isspace() - Əgər sətir yalnızca space-dən ibarətdirsə True qaytarır
print("14. '      '.isspace() =>", '     '.isspace())

# 15. istitle() - Sətrin bütün sözləri böyük hərflə başlayırsa True qaytarır
print("15. 'Salam Dünya'.istitle() =>", 'Salam Dünya'.istitle())

# 16. isupper() - Sətrin bütün hərfləri böyükdürsə True qaytarır
print("16. 'SALAM DÜNYA'.isupper() =>", 'SALAM DÜNYA'.isupper())

# 17. join(seq) - Sətrin simvollarının arasına verilən sözü yerləşdirir
print("17. '='.join('salam dünya') =>", '='.join('salam dünya'))

# 18. lower() - Sətrin bütün simvollarını kiçik edir
print("18. 'SALAM'.lower() =>", 'SALAM'.lower())

# 19. upper() - Sətrin bütün simvollarını böyük edir
print("19. 'salam'.upper() =>", 'salam'.upper())

# 20. strip([chars]) - Sətrin əvvəlindən və sonunu təmizləyir
print("20. '  salam dünya\t'.strip() => ", '   salam dünya\t'.strip())
print("20. 'aarif haciyeva'.strip() => ", 'aarif haciyeva'.strip('a'))

# 21. replace(old, new [, max]) - Mətndə old sözünü new ilə əvəz edir. Max isə limitdir
print("21. myStr.replace('lam', 'LAT') =>", myStr.replace('lam', 'LAT'))

# 22. split(str="", num=string.count(str)) - Sətri verilmiş sözə görə parçalayır
print("22. myStr.split() =>", myStr.split())
print("22. myStr.split('a') =>", myStr.split('a'))










