# What will be the result of the following?

```
>>> t = (1, 2, [30, 40])
>>> t[2] += [50, 60]
```

a) t becomes (1, 2, [30, 40, 50, 60])

b) TypeError is raised with the message `'tuple' object does not support item assignment`

c) Neither

d) Both A and B